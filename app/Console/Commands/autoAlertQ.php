<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Mail\AlertAcceptEmail;

class autoAlertQ extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:alertq';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Alert Accept OR Reject MT Request';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tyear = '2562';
        $rawdata = DB::table('tbl_mt_reqadd')
            ->join('tbl_mt_user', 'tbl_mt_user.emp_id', '=', 'tbl_mt_reqadd.empcode')
            ->select(DB::raw('tbl_mt_reqadd.requestor,tbl_mt_reqadd.empcode,tbl_mt_user.mail,count(tbl_mt_reqadd.id) as job'))
            ->where('tbl_mt_reqadd.date_req','like', '%'.$tyear.'%')
            ->where('tbl_mt_reqadd.status', 4)
            ->where('tbl_mt_reqadd.status_eval')
            ->groupBy(DB::raw('tbl_mt_reqadd.requestor,tbl_mt_reqadd.empcode,tbl_mt_user.mail'))
            ->get();
        $mailObj = array();
        foreach ($rawdata as $obj) {
            $mailObj['alertdata'] = $obj;
            $mailObj['subject'] = "งานค้างในระบบ MT Request ".$obj->requestor;

            //Mail::to('parinya.k@lannaagro.com')->bcc('parinya.k@lannaagro.com')->send(new AlertAcceptEmail($mailObj));
            Mail::to($obj->mail)->cc('somsak@lannaagro.com')->send(new AlertAcceptEmail($mailObj));
        }
    }
}
