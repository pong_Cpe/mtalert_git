<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FT Form Alert</title>
    <style>
        table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        }
    </style>
</head>
<body>
<p><strong>TO..{{ $alertdata['alertdata']->requestor }}</strong></p>
    <p><strong>จาก MT Request Online System</strong></p>
    <p>มีงานค้างที่ตรวจรับในระบบจำนวน {{ $alertdata['alertdata']->job }} งาน</p>
    <p>ในกรณที่ไม่เข้าไปกด ”ยอมรับ” งานหลังจาก MT Close job แล้ว ทาง MT ขอสงวนสิทธิ์ในการ Reject การแจ้งงานในครั้งถัดไปครับ</p>
    <p><a href="http://lacosp1:85">http://lacosp1:85</a></p>
    <br>
    <br>
    <p><strong>MT Request Online System</strong></p>
</body>
</html>